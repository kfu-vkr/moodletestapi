using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MoodleTestAPI.Dtos;
using MoodleTestAPI.Events.Core;
using MoodleTestAPI.Models;
using MoodleTestAPI.Tools;

namespace MoodleTestAPI.Controllers
{
    [ApiController]
    [Route("/api/hooks/[controller]")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class CoreController : ControllerBase
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<CoreController> _logger;
        private readonly IConfiguration _configuration;
        private readonly ControllerValidator _validator;

        public CoreController(
            IServiceProvider services,
            ILogger<CoreController> logger, 
            IConfiguration configuration
            )
        {
            _services = services;
            _logger = logger;
            _configuration = configuration;
            _validator = new ControllerValidator(services);
        }

        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromBody] JsonElement value)
        {
            #region Авторизация
            if (_validator.ValidateEvent(value))
            {
                return BadRequest(new
                {
                    error = true,
                    error_description = "Invalid or empty eventname",
                });
            }

            if (_validator.CheckAuth(value))
            {
                return Unauthorized(new
                {
                    error = true,
                    error_description = "Invalid or empty token",
                });
            }
            #endregion
            
            var eventName = value.GetFieldAsString("eventname");
            switch (eventName.ToLower().Trim())
            {
                case "\\core\\event\\course_updated":
                    new EventCoreCourseUpdated(_services).HandleCoreEventCourseUpdated(value);
                    break;

                // Известные методы, которые мы пока не хотим имплементировать
                case "\\core\\event\\notification_sent":
                    break;
                
                default:
                    _logger.LogWarning($"Dumping unknown event:{Environment.NewLine}{value.ToString(true)}");
                    throw new NotImplementedException($"Event '{eventName}' is not implemented yet");
            }
            
            return Ok(new object());
        }
    }
}
