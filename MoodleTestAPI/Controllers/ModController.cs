using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MoodleTestAPI.Dtos;
using MoodleTestAPI.Events.Core;
using MoodleTestAPI.Events.Mod.Forum;
using MoodleTestAPI.Models;
using MoodleTestAPI.Tools;

namespace MoodleTestAPI.Controllers
{
    [ApiController]
    [Route("/api/hooks/[controller]")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class ModController : ControllerBase
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<ModController> _logger;
        private readonly IConfiguration _configuration;
        private readonly ControllerValidator _validator;

        public ModController(
            IServiceProvider services,
            ILogger<ModController> logger, 
            IConfiguration configuration
            )
        {
            _services = services;
            _logger = logger;
            _configuration = configuration;
            _validator = new ControllerValidator(services);
        }

        [HttpPost]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromBody] JsonElement value)
        {
            #region Авторизация
            if (_validator.ValidateEvent(value))
            {
                return BadRequest(new
                {
                    error = true,
                    error_description = "Invalid or empty eventname",
                });
            }

            if (_validator.CheckAuth(value))
            {
                return Unauthorized(new
                {
                    error = true,
                    error_description = "Invalid or empty token",
                });
            }
            #endregion
            
            var eventName = value.GetFieldAsString("eventname");
            var component = value.GetFieldAsString("component");
            switch (component.ToLower().Trim())
            {
                case "mod_forum":
                    break;
                default:
                    _logger.LogWarning($"Dumping unknown event:{Environment.NewLine}{value.ToString(true)}");
                    throw new NotImplementedException($"Event '{eventName}' is not implemented yet");
            }
            
            switch (eventName.ToLower().Trim())
            {
                case "\\mod_forum\\event\\assessable_uploaded":
                    new EventAssessableUploaded(_services).HandleModForumEventAssessableUploaded(value);
                    break;
                
                case "\\mod_forum\\event\\discussion_created":
                    break;
                
                default:
                    _logger.LogWarning($"Dumping unknown event:{Environment.NewLine}{value.ToString(true)}");
                    throw new NotImplementedException($"Event '{eventName}' is not implemented yet");
            }
            
            return Ok(new object());
        }
    }
}
