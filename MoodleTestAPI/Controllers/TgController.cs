using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MoodleTestAPI.Dtos;
using MoodleTestAPI.Events.Core;
using MoodleTestAPI.Models;
using MoodleTestAPI.Models.Telegram;
using MoodleTestAPI.Tools;
using Telegram.BotAPI.AvailableMethods;

namespace MoodleTestAPI.Controllers
{
    [ApiController]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class TgController : ControllerBase
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<TgController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IVelocityDbService _dbService;
        private readonly ControllerValidator _validator;
        private readonly ITgBotService _tgBotService;

        public TgController(
            IServiceProvider services,
            ILogger<TgController> logger, 
            IConfiguration configuration,
            IVelocityDbService dbService,
            ITgBotService tgBotService
            )
        {
            _services = services;
            _logger = logger;
            _configuration = configuration;
            _dbService = dbService;
            _validator = new ControllerValidator(services);
            _tgBotService = tgBotService;
        }

        [HttpPost("/api/tghooks")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromBody] TGUpdate value)
        {
            if (value.ChannelPost?.Chat == null || string.IsNullOrEmpty(value.ChannelPost.Text))
            {
                // Нет чата? Пустая строка? Не текстовое сообщение?
                // Неизвестный апдейт, например, CallbackQuery. В любом случае, мы это не обрабатываем
                _logger.LogWarning(value.ToString(true, true));
                return Ok();
            }
            
            var msgParts = value.ChannelPost.Text.Split(" ")
                .Select(x => x.Trim())
                .ToArray();
            
            var leftSide = msgParts.FirstOrDefault();
            // Не обрабатываем короткое сообщение
            if (string.IsNullOrEmpty(leftSide) || msgParts.Length < 2)
            {
                return Ok();
            }

            switch (leftSide.ToLower().Trim())
            {
                case "/reg":
                    break;
                default:
                    _tgBotService.GetClient().SendMessage(
                        value.ChannelPost.Chat.Id,
                        $"{leftSide} - неизвестная команда",
                        disableWebPagePreview: true
                        );
                    return Ok();
            }

            var rightSide = msgParts[1];

            var tgChatInfo = _dbService.FindTelegramRegisteredChatLink(rightSide);
            if (tgChatInfo == null)
            {
                // Нет регистрации, завершаем диалог
                _tgBotService.GetClient().SendMessage(
                    value.ChannelPost.Chat.Id,
                    "Данная группа или канал не ожидает регистрации"
                    );
                return Ok();
            }
            else
            {
                // Есть запись, проверяем на наличие chat_id
                if (tgChatInfo.Id != null)
                {
                    _tgBotService.GetClient().SendMessage(
                        value.ChannelPost.Chat.Id,
                        $"Идентификатор {value.ChannelPost.Chat.Id} уже был ранее привязан к каналу {rightSide}",
                        disableWebPagePreview: true
                        );
                    return Ok();
                }

                tgChatInfo.Id = value.ChannelPost.Chat.Id;
                _dbService.RegisterTelegramChatLink(rightSide, value.ChannelPost.Chat.Id,
                    value.ChannelPost.Chat.Type, value.ChannelPost.Chat.Title);
                
                // Сброс дисковых кэшей
                _dbService.FlushUpdates();

                _tgBotService.GetClient().SendMessage(
                    value.ChannelPost.Chat.Id,
                    $"Идентификатор {value.ChannelPost.Chat.Id} успешно привязан к каналу {rightSide}",
                    disableWebPagePreview: true
                    );
            }
            
            //_logger.LogInformation(tgChatInfo.ToString(true, true));
            return Ok(new object());
        }
    }
}
