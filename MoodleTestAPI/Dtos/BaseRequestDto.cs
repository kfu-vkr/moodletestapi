#nullable enable
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using MoodleTestAPI.Tools;

#pragma warning disable 8618

namespace MoodleTestAPI.Dtos
{
    [DataContract]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class BaseRequestUserDto
    {
        [JsonPropertyName("id")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? Id { get; set; }
        
        [JsonPropertyName("auth")]
        public string Auth { get; set; }
        
        [JsonPropertyName("confirmed")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool Confirmed { get; set; }

        [JsonPropertyName("policyagreed")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool PolicyAgreed { get; set; }

        [JsonPropertyName("deleted")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool Deleted { get; set; }
        
        [JsonPropertyName("suspended")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool Suspended { get; set; }

        [JsonPropertyName("mnethostid")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? MNetHostId { get; set; }
        
        [JsonPropertyName("username")]
        public string UserName { get; set; }

        [JsonPropertyName("idnumber")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? IdNumber { get; set; }

        [JsonPropertyName("firstname")]
        public string FirstName { get; set; }

        [JsonPropertyName("lastname")]
        public string LastName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }
        
        [JsonPropertyName("emailstop")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool EmailStop { get; set; }

        [JsonPropertyName("icq")]
        public string? ICQ { get; set; }

        [JsonPropertyName("skype")]
        public string? Skype { get; set; }

        [JsonPropertyName("yahoo")]
        public string? Yahoo { get; set; }

        [JsonPropertyName("aim")]
        public string? AIM { get; set; }

        [JsonPropertyName("msn")]
        public string? MSN { get; set; }

        [JsonPropertyName("phone1")]
        public string? Phone1 { get; set; }

        [JsonPropertyName("phone2")]
        public string? Phone2 { get; set; }

        [JsonPropertyName("institution")]
        public string? Institution { get; set; }

        [JsonPropertyName("department")]
        public string? Department { get; set; }

        [JsonPropertyName("city")]
        public string? City { get; set; }

        [JsonPropertyName("country")]
        public string? Country { get; set; }

        [JsonPropertyName("lang")]
        public string? Lang { get; set; }

        [JsonPropertyName("calendartype")]
        public string? CalendarType { get; set; }

        [JsonPropertyName("theme")]
        public string? Theme { get; set; }

        [JsonPropertyName("timezone")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? Timezone { get; set; }

        [JsonPropertyName("firstaccess")]
        [JsonConverter(typeof(ParseDatetimeNullableFieldJsonConverter))]
        public DateTime? FirstAccess { get; set; }

        [JsonPropertyName("lastaccess")]
        [JsonConverter(typeof(ParseDatetimeNullableFieldJsonConverter))]
        public DateTime? LastAccess { get; set; }

        [JsonPropertyName("lastlogin")]
        [JsonConverter(typeof(ParseDatetimeNullableFieldJsonConverter))]
        public DateTime? LastLogin { get; set; }

        [JsonPropertyName("currentlogin")]
        [JsonConverter(typeof(ParseDatetimeNullableFieldJsonConverter))]
        public DateTime? CurrentLogin { get; set; }

        [JsonPropertyName("lastip")]
        public string LastIP { get; set; }

        [JsonPropertyName("secret")]
        public string? Secret { get; set; }

        [JsonPropertyName("picture")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? Picture { get; set; }
        
        [JsonPropertyName("url")]
        public string? URL { get; set; }
        
        [JsonPropertyName("descriptionformat")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? DescriptionFormat { get; set; }
        
        [JsonPropertyName("mailformat")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? MailFormat { get; set; }
        
        [JsonPropertyName("maildigest")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool? MailDigest { get; set; }
        
        [JsonPropertyName("maildisplay")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? MailDisplay { get; set; }
        
        [JsonPropertyName("autosubscribe")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool? AutoSubscribe { get; set; }
        
        [JsonPropertyName("trackforums")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool? TrackForums { get; set; }

        [JsonPropertyName("timecreated")]
        [JsonConverter(typeof(ParseDatetimeNullableFieldJsonConverter))]
        public DateTime? TimeCreated { get; set; }

        [JsonPropertyName("timemodified")]
        [JsonConverter(typeof(ParseDatetimeNullableFieldJsonConverter))]
        public DateTime? TimeModified { get; set; }

        [JsonPropertyName("trustbitmask")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? TrustBitMask { get; set; }
        
        [JsonPropertyName("imagealt")]
        public string? ImageAlt { get; set; }
        
        [JsonPropertyName("lastnamephonetic")]
        public string? LastNamePhonetic { get; set; }
        
        [JsonPropertyName("firstnamephonetic")]
        public string? FirstNamePhonetic { get; set; }
        
        [JsonPropertyName("middlename")]
        public string? MiddleName { get; set; }
        
        [JsonPropertyName("alternatename")]
        public string? AlternateName { get; set; }
        
        [JsonPropertyName("moodlenetprofile")]
        public string? moodlenetprofile { get; set; }
        
        [JsonPropertyName("lastcourseaccess")]
        public object? LastCourseAccess { get; set; }
        
        [JsonPropertyName("currentcourseaccess")]
        public object? CurrentCourseAccess { get; set; }
        
        [JsonPropertyName("groupmember")]
        public List<object> GroupMember { get; set; }
        
        [JsonPropertyName("profile")]
        public List<object> Profile { get; set; }
        
        [JsonPropertyName("sesskey")]
        public string? SessKey { get; set; }

        [JsonPropertyName("ajax_updatable_user_prefs")]
        public object? AjaxUpdatableUserPrefs { get; set; }

        [JsonPropertyName("access")]
        public object? Access { get; set; }

        [JsonPropertyName("enrol")]
        public object? Enrol { get; set; }
        
        [JsonPropertyName("editing")]
        [JsonConverter(typeof(BoolFieldJsonConverter))]
        public bool? Editing { get; set; }
        
        [JsonPropertyName("grade_last_report")]
        public object? GradeLastReport { get; set; }

        [JsonPropertyName("gradeediting")]
        public object? GradeEditing { get; set; }
    }

    
    [DataContract]
    public class BaseRequestDto
    {
        [Required]
        [JsonPropertyName("eventname")] 
        public string EventName { get; set; }
        
        [JsonPropertyName("component")] 
        public string Component { get; set; }
        
        [JsonPropertyName("action")]
        public string Action { get; set; }

        [JsonPropertyName("target")]
        public string Target { get; set; }

        [JsonPropertyName("objecttable")]
        public string ObjectTable { get; set; }

        [JsonPropertyName("objectid")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? ObjectId { get; set; }

        [JsonPropertyName("crud")]
        public string Crud { get; set; }
        
        [JsonPropertyName("edulevel")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? EduLevel { get; set; }
        
        [JsonPropertyName("contextid")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? ContextId { get; set; }
        
        [JsonPropertyName("contextlevel")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? ContextLevel { get; set; }
        
        [JsonPropertyName("contextinstanceid")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? ContextInstanceId { get; set; }
        
        [JsonPropertyName("userid")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? UserId { get; set; }
        
        [JsonPropertyName("courseid")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? CourseId { get; set; }
        
        [JsonPropertyName("relateduserid")]
        [JsonConverter(typeof(IntNullableFieldJsonConverter))]
        public int? RelatedUserId { get; set; }

        [JsonPropertyName("anonymous")]
        public int Anonymous { get; set; }
        
        [JsonPropertyName("timecreated")]
        [JsonConverter(typeof(ParseDatetimeNullableFieldJsonConverter))]
        public DateTime? TimeCreated { get; set; }

        [JsonPropertyName("host")]
        public string Host { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }
        
        [JsonPropertyName("extra")]
        public string Extra { get; set; }
        
        [JsonPropertyName("user")]
        //public object? User { get; set; }
        public BaseRequestUserDto User { get; set; }
    }
}
