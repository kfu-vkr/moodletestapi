using Microsoft.AspNetCore.Mvc;

namespace MoodleTestAPI.Dtos
{
    public static class BaseResponse
    {
        public static object Empty => new { };
        public static object StatusOk => new OkObjectResult(new
        {
            result = "ok",
        });
    }
}