using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace MoodleTestAPI.Dtos
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class CoreEventCourseUpdatedDto : BaseRequestDto
    {
        public class OtherObject
        {
            /// <summary>
            /// </summary>
            [JsonPropertyName("shortname")]
            public string? ShortName { get; set; }

            /// <summary>
            /// </summary>
            [JsonPropertyName("fullname")]
            public string? FullName { get; set; }

            public class UpdatedFieldsObject
            {
                /// <summary>
                /// </summary>
                [JsonPropertyName("idnumber")]
                public string? IDNumber { get; set; }
            }
            
            /// <summary>
            /// </summary>
            [JsonPropertyName("updatedfields")]
            public UpdatedFieldsObject? UpdatedFields { get; set; }
        }
        
        [JsonPropertyName("other")]
        public OtherObject? Other { get; set; }
    }
}