using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace MoodleTestAPI.Dtos
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ModForumRequestDto : BaseRequestDto
    {
        public class OtherObject
        {
            /// <summary>
            /// </summary>
            [JsonPropertyName("content")]
            public string? Content { get; set; }

            /// <summary>
            /// </summary>
            [JsonPropertyName("pathnameshashes")]
            public string[]? PathNamesHashes { get; set; }

            /// <summary>
            /// </summary>
            [JsonPropertyName("discussionid")]
            public int? DiscussionId { get; set; }

            /// <summary>
            /// </summary>
            [JsonPropertyName("triggeredfrom")]
            public string? TriggeredFrom { get; set; }

        }
        
        [JsonPropertyName("other")]
        public OtherObject? Other { get; set; }
    }
}