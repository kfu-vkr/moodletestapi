using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MoodleTestAPI.Dtos;
using MoodleTestAPI.Models;
using MoodleTestAPI.Tools;

namespace MoodleTestAPI.Events.Core
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class EventCoreCourseUpdated
    {
        private readonly ILogger<EventCoreCourseUpdated> _logger;
        private readonly IVelocityDbService _dbService;
        
        public EventCoreCourseUpdated(IServiceProvider services)
        {
            var scope = services.CreateScope();
            _logger = scope.ServiceProvider.GetRequiredService<ILogger<EventCoreCourseUpdated>>();
            scope.ServiceProvider.GetRequiredService<IConfiguration>();
            scope.ServiceProvider.GetRequiredService<IHostEnvironment>();
            _dbService = scope.ServiceProvider.GetRequiredService<IVelocityDbService>();
        }

        public void HandleCoreEventCourseUpdated(JsonElement element)
        {
            HandleCoreEventCourseUpdated(element.GetRawText()
                .Trim()
                .ToReqDto<CoreEventCourseUpdatedDto>()
            );
        }
        public void HandleCoreEventCourseUpdated(CoreEventCourseUpdatedDto? webDto)
        {
            if (webDto == null)
            {
                _logger.LogError("CoreEventCourseUpdatedDto is null");
                throw new ArgumentException();
            }

            var dbDto = new CourseInfo
            {
                CourseId = webDto.CourseId ?? -1,
                ShortName = webDto.Other?.ShortName,
                FullName = webDto.Other?.FullName,
                TGLinkUrl = webDto.Other?.UpdatedFields?.IDNumber,
            };

            // Обновление метаданных курса
            _dbService.UpdateCourse(dbDto);
            // Обновления регистрации чат-линков
            _dbService.RegisterTelegramChatLink(webDto.Other?.UpdatedFields?.IDNumber);
            // Сброс дисковых кэшей
            _dbService.FlushUpdates();
        }
    }
}
