using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MoodleTestAPI.Dtos;
using MoodleTestAPI.Models.Telegram;
using MoodleTestAPI.Tools;
using Telegram.BotAPI.AvailableMethods;

namespace MoodleTestAPI.Events.Mod.Forum
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class EventAssessableUploaded
    {
        private readonly ILogger<EventAssessableUploaded> _logger;
        private readonly IVelocityDbService _dbService;
        private readonly ITgBotService _tgBotService;

        public EventAssessableUploaded(IServiceProvider services)
        {
            var scope = services.CreateScope();
            _logger = scope.ServiceProvider.GetRequiredService<ILogger<EventAssessableUploaded>>();
            scope.ServiceProvider.GetRequiredService<IConfiguration>();
            scope.ServiceProvider.GetRequiredService<IHostEnvironment>();
            _dbService = scope.ServiceProvider.GetRequiredService<IVelocityDbService>();
            _tgBotService = scope.ServiceProvider.GetRequiredService<ITgBotService>();
        }
        
        public void HandleModForumEventAssessableUploaded(JsonElement element)
        {
            HandleModForumEventAssessableUploaded(element.GetRawText()
                .Trim()
                .ToReqDto<ModForumRequestDto>()
            );
        }

        public void HandleModForumEventAssessableUploaded(ModForumRequestDto? webDto)
        {
            if (webDto == null)
            {
                _logger.LogError("ModForumRequestDto is null");
                throw new ArgumentException();
            }

            var course = _dbService.FindCourse(webDto.CourseId);
            if (course == null)
            {
                _logger.LogInformation($"Can't find course id {webDto.CourseId}");
                return;
            }
            
            #region Дамп объявления в консоль
            var msg = $"Дамп объявления:{Environment.NewLine}" +
                      $"---------------------------------------------{Environment.NewLine}" +
                      $"Короткое название курса: {course.ShortName}{Environment.NewLine}" +
                      $"Полное название курса: {course.FullName}{Environment.NewLine}" +
                      $"Telegram-канал курса: {course.TGLinkUrl}{Environment.NewLine}" +
                      $"Объявление (HTML): {Environment.NewLine}{Environment.NewLine}" +
                      webDto.Other?.Content + Environment.NewLine + Environment.NewLine +
                      $"Объявление (TEXT): {Environment.NewLine}{Environment.NewLine}" +
                      webDto.Other?.Content?.StripHTML();
            _logger.LogInformation(msg);
            #endregion
            #region Проверка чат-линка
            var tgChatInfo = _dbService.FindTelegramRegisteredChatLink(course.TGLinkUrl);
            if (tgChatInfo == null)
            {
                return;
            }

            msg = $"<b>Объявление</b>{Environment.NewLine}{Environment.NewLine}" +
                  webDto.Other?.Content?.Trim().StripHTML(false).Trim(); 
              
            _logger.LogWarning($"Sending to telegram: {Environment.NewLine}{msg}");
            var res = _tgBotService.GetClient().SendMessage(
                chatId: tgChatInfo.Id,
                text: msg,
                parseMode: "HTML"
            );
            
            _logger.LogError(res.ToString(true));
            
            #endregion
        }
    }
}
