using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using LiteDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MoodleTestAPI.Models;
using MoodleTestAPI.Models.Telegram;
using MoodleTestAPI.Tools;

namespace MoodleTestAPI
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface ILiteDbService
    {
        /// <summary>
        /// Получение экземпляра класса <see cref="LiteDatabase"/>
        /// </summary>
        /// <returns>Экземпляр класса <see cref="LiteDatabase"/></returns>
        public LiteDatabase? GetEngine();
        
        /// <summary>
        /// Поиск курса по идентификатору курса и объекта (опционально)
        /// </summary>
        /// <param name="CourseId">идентификатор курса</param>
        /// <returns>Экземпляр класса <see cref="CourseInfo"/> или null</returns>
        public CourseInfo? FindCourse(int CourseId);

        /// <summary>
        /// Обновление метаданных курса в базе данных
        /// </summary>
        /// <remarks>Если данного курса нет, он создастся, в противном случае обновится</remarks>
        /// <param name="CourseId">идентификатор курса</param>
        /// <param name="ShortName">короткое имя курса</param>
        /// <param name="FullName">полное имя курса</param>
        /// <param name="TGLinkUrl">группа или канал в Telegram</param>
        public void UpdateCourse(int CourseId, 
            string? ShortName = default, string? FullName = default, string? TGLinkUrl = default);
        
        /// <summary>
        /// Обновление курса в базе данных
        /// </summary>
        /// <remarks>Если данного курса нет, он создастся, в противном случае обновится</remarks>
        /// <param name="courseInfo">Экземпляр класса <see cref="CourseInfo"/></param>
        public void UpdateCourse(CourseInfo? courseInfo);

        /// <summary>
        /// Регистрация ссылки Telegram-чата в базе данных
        /// </summary>
        /// <param name="chatInfo"></param>
        public void RegisterTelegramChatLink(TGChatInfo chatInfo);

        /// <summary>
        /// Регистрация ссылки Telegram-чата в базе данных
        /// </summary>
        /// <param name="chatLink"></param>
        /// <param name="chatId"></param>
        /// <param name="type"></param>
        /// <param name="title"></param>
        public void RegisterTelegramChatLink(string? chatLink, long? chatId = default,
            string? type = default, string? title = default);

        /// <summary>
        /// Поиск экземпляра класса <see cref="TGChatInfo"/> по ссылке Telegram-чата
        /// </summary>
        /// <param name="chatLink"></param>
        /// <returns>Экземпляр класса <see cref="TGChatInfo"/></returns>
        public TGChatInfo? FindTelegramRegisteredChatLink(string? chatLink);

    }
}
