using System.Diagnostics.CodeAnalysis;
using LiteDB;
using Telegram.BotAPI;

namespace MoodleTestAPI
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface ITgBotService
    {
        /// <summary>
        /// Получение экземпляра класса <see cref="BotClient"/>
        /// </summary>
        /// <returns>Экземпляр класса <see cref="BotClient"/></returns>
        public BotClient? GetClient();
        
        // /// <summary>
        // /// Поиск курса по идентификатору курса и объекта (опционально)
        // /// </summary>
        // /// <param name="CourseId">идентификатор курса</param>
        // /// <param name="ObjectId">идентификатор объекта</param>
        // /// <returns>Экземпляр класса <see cref="CourseInfo"/> или null</returns>
        // public CourseInfo? FindCourse(int CourseId, int? ObjectId = default);
        //
        // /// <summary>
        // /// Обновление метаданных курса в базе данных
        // /// </summary>
        // /// <remarks>Если данного курса нет, он создастся, в противном случае обновится</remarks>
        // /// <param name="CourseId">идентификатор курса</param>
        // /// <param name="ObjectId">идентификатор объекта</param>
        // /// <param name="ShortName">короткое имя курса</param>
        // /// <param name="FullName">полное имя курса</param>
        // /// <param name="TGLinkUrl">группа или канал в Telegram</param>
        // public void UpdateCourse(int CourseId, int? ObjectId = default, 
        //     string? ShortName = default, string? FullName = default, string? TGLinkUrl = default);
        //
        // /// <summary>
        // /// Обновление курса в базе данных
        // /// </summary>
        // /// <remarks>Если данного курса нет, он создастся, в противном случае обновится</remarks>
        // /// <param name="courseInfo">Экземпляр класса <see cref="CourseInfo"/></param>
        // public void UpdateCourse(CourseInfo? courseInfo);
    }
}
