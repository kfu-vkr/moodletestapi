using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using LiteDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Writers;
using MoodleTestAPI.Models;
using MoodleTestAPI.Models.Telegram;
using MoodleTestAPI.Tools;
using Telegram.BotAPI;
#pragma warning disable 8602

namespace MoodleTestAPI
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class LiteDbService : ILiteDbService
    {
        private readonly LiteDatabase? _dbEngine;
        private readonly IConfiguration _configuration;
        private readonly ILogger<LiteDbService> _logger;
        private readonly IHostEnvironment _env;
        
        public LiteDbService(
            IConfiguration configuration,
            ILogger<LiteDbService> logger,
            IHostEnvironment environment
            )
        {
            _configuration = configuration;
            _logger = logger;
            _env = environment;
            var dbPath = Path.Join(IOTools.GetRunningDir(_env), 
                configuration.GetValue("DBPath", ""));

            if (!Directory.Exists(Path.GetDirectoryName(dbPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(dbPath)!);
            }

            _dbEngine = new LiteDatabase(dbPath);
        }
        
        public LiteDatabase? GetEngine()
        {
            return _dbEngine;
        }

        public CourseInfo? FindCourse(int CourseId)
        {
            if (CourseId <= 0)
            {
                return null;
            }
            
            var kv = _dbEngine?.GetCollection<CourseInfo>("CourseInfo");
            return kv?.FindOne(x => x.CourseId == CourseId);
        }

        public void UpdateCourse(int CourseId, string? ShortName = default, string? FullName = default, 
            string? TGLinkUrl = default)
        {
            var courseInfo = new CourseInfo
            {
                CourseId = CourseId,
                ShortName = ShortName,
                FullName = FullName,
                TGLinkUrl = TGLinkUrl
            };
            
            UpdateCourse(courseInfo);
        }
        
        public void UpdateCourse(CourseInfo? courseInfo)
        {
            if (courseInfo is not {CourseId: > 0})
            {
                _logger.LogError("DB: Course info is null or CourseId is wrong");
                return;
            }

            var kv = _dbEngine?.GetCollection<CourseInfo>("CourseInfo");
            var ci = kv.FindOne(x => x.CourseId == courseInfo.CourseId);
            if (ci != null)
            {
                //kv.Delete()
                _logger.LogInformation($"DB: Course \"{courseInfo.FullName}\" info updated");
                kv.Update(ci);
            }
            else
            {
                _logger.LogInformation($"DB: Course \"{courseInfo.FullName}\" info updated");
                kv.Insert(new[]{courseInfo});
            }
        }

        public void RegisterTelegramChatLink(TGChatInfo chatInfo)
        {
            RegisterTelegramChatLink(chatInfo.ChatLink, chatInfo.Id, chatInfo.Type, chatInfo.Title);
        }
        
        public void RegisterTelegramChatLink(string? chatLink, long? chatId = default,
            string? type = default, string? title = default)
        {
            if (string.IsNullOrEmpty(chatLink))
            {
                return;
            }
                
            var dbDto = new TGChatInfo
            {
                ChatLink = chatLink,
                Id = chatId,
                Type = type,
                Title = title,
            };
            
            var kv = _dbEngine?.GetCollection<TGChatInfo>("TgRegChats");
            var chatSession = kv?.FindOne(x => 
                x.ChatLink.Equals(chatLink.Trim(), StringComparison.InvariantCultureIgnoreCase));
            if (chatSession != null)
            {
                kv?.Update(dbDto);
            }
            else
            {
                var res = kv?.Insert(dbDto);
            }
        }

        public TGChatInfo? FindTelegramRegisteredChatLink(string? chatLink)
        {

            var keys = _dbEngine?.GetCollectionNames();
            
            var kv = _dbEngine?.GetCollection<TGChatInfo>("TgRegChats");
            return kv?.FindOne(x => 
                x.ChatLink.Equals(chatLink!.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        // public long? FindTgChatIdByChatLink(string chatLink)
        // {
        //     var dbDtoSum = "ZZZ" + chatLink.GetMD5String();
        //     var kv = _dbEngine?.GetCollection<long>(dbDtoSum);
        //     return kv?.FindOne(x => x != long.MaxValue);
        // }
        //
        // public void BindTgChatLinkToChatId(string chatLink, long chatId)
        // {
        //     var dbDtoSum = "ZZZ" + chatLink.GetMD5String();
        //     var kv = _dbEngine?.GetCollection<long>(dbDtoSum);
        //     kv?.Insert(chatId);
        // }
        
    }
}
