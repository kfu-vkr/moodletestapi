using System.Diagnostics.CodeAnalysis;

namespace MoodleTestAPI.Models
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class CourseInfo
    {
        /// <summary>
        /// </summary>
        public int CourseId { get; set; }
        
        /// <summary>
        /// </summary>
        public string? ShortName { get; set; }
        
        /// <summary>
        /// </summary>
        public string? FullName { get; set; }
        
        /// <summary>
        /// </summary>
        public string? TGLinkUrl { get; set; }
    }
}