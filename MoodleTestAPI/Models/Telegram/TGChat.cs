using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
#pragma warning disable 8618

namespace MoodleTestAPI.Models.Telegram
{
    [DataContract]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class TGChat
    {
        [JsonPropertyName("id")]
        public long Id { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("title")]
        public string? Title { get; set; }

        [JsonPropertyName("username")]
        public string? Username { get; set; }
        
        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }

        [JsonPropertyName("last_name")]
        public string? LastName { get; set; }
        
        [JsonPropertyName("photo")]
        public JsonElement? Photo { get; set; }
        
        [JsonPropertyName("bio")]
        public string? BIO { get; set; }
        
        [JsonPropertyName("description")]
        public string? Description { get; set; }
        
        [JsonPropertyName("invite_link")]
        public string? InviteLink { get; set; }
        
        [JsonPropertyName("pinned_message")]
        public TGMessage? PinnedMessage { get; set; }
        
        [JsonPropertyName("permissions")]
        public JsonElement? Permissions { get; set; }
        
        [JsonPropertyName("slow_mode_delay")]
        public int? SlowModeDelay { get; set; }
        
        [JsonPropertyName("message_auto_delete_time")]
        public int? MessageAutoDeleteTime { get; set; }
        
        [JsonPropertyName("sticker_set_name")]
        public string? StickerSetName { get; set; }

        [JsonPropertyName("can_set_sticker_set")]
        public bool? CanSetStickerSet { get; set; }
        
        [JsonPropertyName("linked_chat_id")]
        public long? LinkedChatId { get; set; }

        [JsonPropertyName("location")]
        public TGChatLocation? Location { get; set; }
    }
}