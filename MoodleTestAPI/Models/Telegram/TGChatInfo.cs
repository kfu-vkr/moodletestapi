using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
#pragma warning disable 8618

namespace MoodleTestAPI.Models.Telegram
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class TGChatInfo
    {
        public long? Id { get; set; }
        public string ChatLink { get; set; }
        public string? Type { get; set; }
        public string? Title { get; set; }
    }
}