using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
#pragma warning disable 8618

namespace MoodleTestAPI.Models.Telegram
{
    [DataContract]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class TGLocation
    {
        [JsonPropertyName("longitude")]
        public float Longitude { get; set; }

        [JsonPropertyName("latitude")]
        public float Latitude { get; set; }

        [JsonPropertyName("horizontal_accuracy")]
        public float? HorizontalAccuracy { get; set; }

        [JsonPropertyName("live_period")]
        public long? LivePeriod { get; set; }

        [JsonPropertyName("heading")]
        public int? heading { get; set; }

        [JsonPropertyName("proximity_alert_radius")]
        public int? ProximityAlertRadius { get; set; }
    }
}