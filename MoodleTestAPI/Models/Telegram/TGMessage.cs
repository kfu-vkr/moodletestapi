using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
using Telegram.BotAPI;

#pragma warning disable 8618

namespace MoodleTestAPI.Models.Telegram
{
    [DataContract]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class TGMessage
    {
        [JsonPropertyName("message_id")]
        public long MessageId { get; set; }

        [JsonPropertyName("from")]
        public TGUser? From { get; set; }

        [JsonPropertyName("sender_chat")]
        public TGChat? SenderChat { get; set; }
        
        [JsonPropertyName("date")]
        public long Date { get; set; }
        
        [JsonPropertyName("chat")]
        public TGChat? Chat { get; set; }
        
        [JsonPropertyName("forward_from")]
        public TGUser? ForwardFrom { get; set; }

        [JsonPropertyName("forward_from_chat")]
        public TGChat? ForwardFromChat { get; set; }

        [JsonPropertyName("forward_from_message_id")]
        public long? ForwardFromMessageId { get; set; }
        
        [JsonPropertyName("forward_signature")]
        public string? ForwardDignature { get; set; }
        
        [JsonPropertyName("forward_sender_name")]
        public string? ForwardSenderName { get; set; }
        
        [JsonPropertyName("forward_date")]
        public long? ForwardDate { get; set; }

        [JsonPropertyName("reply_to_message")]
        public TGMessage? ReplyToMessage { get; set; }   
        
        [JsonPropertyName("via_bot")]
        public TGUser? ViaBot { get; set; }   
        
        [JsonPropertyName("edit_date")]
        public long? EditDate { get; set; }
        
        [JsonPropertyName("media_group_id")]
        public string? MediaGroupId { get; set; }
        
        [JsonPropertyName("author_signature")]
        public string? AuthorSignature { get; set; }
        
        [JsonPropertyName("text")]
        public string? Text { get; set; }

        [JsonPropertyName("entities")]
        public TGMessageEntity[]? Entities { get; set; }

        [JsonPropertyName("animation")]
        public JsonElement? Animation { get; set; }

        [JsonPropertyName("audio")]
        public JsonElement? Audio { get; set; }

        [JsonPropertyName("document")]
        public JsonElement? Document { get; set; }

        [JsonPropertyName("photo")]
        public JsonElement[]? Photo { get; set; }

        [JsonPropertyName("sticker")]
        public JsonElement? Sticker { get; set; }

        [JsonPropertyName("video")]
        public JsonElement? Video { get; set; }

        [JsonPropertyName("video_note")]
        public JsonElement? VideoNote { get; set; }

        [JsonPropertyName("voice")]
        public JsonElement? Voice { get; set; }

        [JsonPropertyName("caption")]
        public string? Caption { get; set; }

        [JsonPropertyName("caption_entities")]
        public JsonElement[]? CaptionEntities { get; set; }
        
        [JsonPropertyName("contact")]
        public JsonElement? Contact { get; set; }
        
        [JsonPropertyName("dice")]
        public JsonElement? Dice { get; set; }
        
        [JsonPropertyName("game")]
        public JsonElement? Game { get; set; }
        
        [JsonPropertyName("poll")]
        public JsonElement? Poll { get; set; }
        
        [JsonPropertyName("venue")]
        public JsonElement? Venue { get; set; }
        
        [JsonPropertyName("location")]
        public TGLocation? Location { get; set; }
        
        [JsonPropertyName("new_chat_members")]
        public TGUser[]? NewChatMembers { get; set; }
        
        [JsonPropertyName("left_chat_member")]
        public TGUser? LeftChatMember { get; set; }
        
        [JsonPropertyName("new_chat_title")]
        public string? NewChatTitle { get; set; }
        
        [JsonPropertyName("new_chat_photo")]
        public JsonElement[]? new_chat_photo { get; set; }
        
        [JsonPropertyName("delete_chat_photo")]
        public bool? DeleteChatPhoto { get; set; }
        
        [JsonPropertyName("group_chat_created")]
        public bool? GroupChatCreated { get; set; }
        
        [JsonPropertyName("supergroup_chat_created")]
        public bool? SupergroupChatCreated { get; set; }
        
        [JsonPropertyName("channel_chat_created")]
        public bool? ChannelChatCreated { get; set; }
        
        [JsonPropertyName("message_auto_delete_timer_changed")]
        public JsonElement? MessageAutoDeleteTimerChanged { get; set; }
        
        [JsonPropertyName("migrate_to_chat_id")]
        public long? MigrateToChatId { get; set; }
        
        [JsonPropertyName("migrate_from_chat_id")]
        public long? MigrateFromChatId { get; set; }
        
        [JsonPropertyName("pinned_message")]
        public TGMessage? PinnedMessage { get; set; }
        
        [JsonPropertyName("invoice")]
        public JsonElement? Invoice { get; set; }
        
        [JsonPropertyName("successful_payment")]
        public JsonElement? SuccessfulPayment { get; set; }
        
        [JsonPropertyName("connected_website")]
        public string? ConnectedWebsite { get; set; }
        
        [JsonPropertyName("passport_data")]
        public JsonElement? PassportData { get; set; }

        [JsonPropertyName("proximity_alert_triggered")]
        public JsonElement? ProximityAlertTriggered { get; set; }
        
        [JsonPropertyName("voice_chat_scheduled")]
        public JsonElement? VoiceChatScheduled { get; set; }
        
        [JsonPropertyName("voice_chat_started")]
        public JsonElement? VoiceChatStarted { get; set; }
        
        [JsonPropertyName("voice_chat_ended")]
        public JsonElement? VoiceChatEnded { get; set; }
        
        [JsonPropertyName("voice_chat_participants_invited")]
        public JsonElement? VoiceChatParticipantsInvited { get; set; }
        
        [JsonPropertyName("reply_markup")]
        public JsonElement? ReplyMarkup { get; set; }
    }
}