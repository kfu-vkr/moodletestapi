using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
using Telegram.BotAPI;

#pragma warning disable 8618

namespace MoodleTestAPI.Models.Telegram
{
    [DataContract]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class TGMessageEntity
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("offset")]
        public long Offset { get; set; }

        [JsonPropertyName("length")]
        public long Length { get; set; }

        [JsonPropertyName("url")]
        public string? Url { get; set; }
        
        [JsonPropertyName("user")]
        public TGUser? User { get; set; }
        
        [JsonPropertyName("language")]
        public string? language { get; set; }
    }
}