using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
#pragma warning disable 8618

namespace MoodleTestAPI.Models.Telegram
{
    [DataContract]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class TGUpdate
    {
        [JsonPropertyName("update_id")]
        public long UpdateId { get; set; }

        [JsonPropertyName("message")]
        public TGMessage? Message { get; set; }

        [JsonPropertyName("edited_message")]
        public TGMessage? EditedMessage { get; set; }

        [JsonPropertyName("channel_post")]
        public TGMessage? ChannelPost { get; set; }

        [JsonPropertyName("edited_channel_post")]
        public TGMessage? EditedChannelPost { get; set; }

        [JsonPropertyName("inline_query")]
        public JsonElement? InlineQuery { get; set; }

        [JsonPropertyName("chosen_inline_result")]
        public JsonElement? ChosenInlineResult { get; set; }

        [JsonPropertyName("callback_query")]
        public JsonElement? CallbackQuery { get; set; }

        [JsonPropertyName("shipping_query")]
        public JsonElement? ShippingQuery { get; set; }

        [JsonPropertyName("pre_checkout_query")]
        public JsonElement? PreCheckoutQuery { get; set; }

        [JsonPropertyName("poll")]
        public JsonElement? Poll { get; set; }

        [JsonPropertyName("poll_answer")]
        public JsonElement? PollAnswer { get; set; }

        [JsonPropertyName("my_chat_member")]
        public JsonElement? MyChatMember { get; set; }

        [JsonPropertyName("chat_member")]
        public JsonElement? ChatMember { get; set; }
    }
}