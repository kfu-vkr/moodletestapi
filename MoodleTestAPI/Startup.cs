using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MoodleTestAPI.Tools;

namespace MoodleTestAPI
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _environment;
        public Startup(
            IConfiguration configuration, 
            IWebHostEnvironment environment
            )
        {
            _configuration = configuration;
            _environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressInferBindingSourcesForParameters = true;
                    options.SuppressModelStateInvalidFilter = true;
                    options.SuppressMapClientErrors = true;
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNamingPolicy = new CustomPropertyNamingPolicy();
                    options.JsonSerializerOptions.WriteIndented = _environment.IsDevelopment();
                    options.JsonSerializerOptions.DictionaryKeyPolicy = new SnakeCaseDictionaryKeyPolicy();
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });
            
            //services.AddScoped<ILiteDbService, LiteDbService>();
            //services.AddSingleton<ILiteDbService, LiteDbService>();

            services.AddSingleton<IVelocityDbService, VelocityDbService>();
            
            //services.AddScoped<IVelocityDbService, VelocityDbService>();
            services.AddScoped<ITgBotService, TgBotService>();

            // if (_configuration.GetValue("UseAuth", false))
            // {
            //     services.AddAuthorization(options =>
            //     {
            //         var token = _configuration.GetSection("AppSettings:ApiToken").Get<string>()
            //             .ToLower().Trim();
            //         options.AddPolicy("WebHookAuthToken", builder =>
            //         {
            //             builder.Requirements.Add(new PermissionAuthorizationRequirement(token));
            //         });
            //     });
            // }

            
            services.AddRouting(options =>
                {
                    options.LowercaseUrls = true;
                    options.LowercaseQueryStrings = true;
                })
                .AddMvc(options =>
                {
                    options.Filters.Add(typeof(JsonExceptionFilter));
                });
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "MoodleTestAPI", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(options => 
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "MoodleTestAPI v1"));
            }

            if (_configuration.GetValue("HTTPSOnly", false))
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();

            if (_configuration.GetValue("UseAuth", false))
            {
                app.UseAuthentication();
                app.UseAuthorization();
            }

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}