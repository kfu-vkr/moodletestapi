using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net.Http;
using LiteDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Writers;
using Microsoft.VisualBasic;
using MoodleTestAPI.Models;
using MoodleTestAPI.Tools;
using Telegram.BotAPI;
using Telegram.BotAPI.GettingUpdates;

namespace MoodleTestAPI
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    public class TgBotService : ITgBotService
    {
        private readonly LiteDatabase? _dbEngine;
        private readonly IConfiguration _configuration;
        private readonly ILogger<TgBotService> _logger;
        private readonly IHostEnvironment _env;
        private readonly BotClient _botClient;
        
        public TgBotService(
            IConfiguration configuration,
            ILogger<TgBotService> logger,
            IHostEnvironment environment
            )
        {
            _configuration = configuration;
            _logger = logger;
            _env = environment;

            var botToken = _configuration.GetSection("AppSettings:TgBotApiToken").Get<string>();
            _botClient = new BotClient(botToken);
            _botClient.IgnoreBotExceptions = true;
            
            var useTgBotWebHook = _configuration.GetSection("AppSettings:UseTgBotWebHook").Get<bool>();
            if (!useTgBotWebHook)
            {
                return;
            }
            var tgWebHookUrl = _configuration.GetSection("AppSettings:TgBotWebHookUrl").Get<string>();
            var webhookInfo = _botClient.GetWebhookInfo();
            if (!string.IsNullOrEmpty(webhookInfo.Url) && webhookInfo.Url.Equals(tgWebHookUrl))
            {
                return;
            }
            
            var res = _botClient.SetWebhook(tgWebHookUrl);
            if (!res)
            {
                _logger.LogError("SetWebhook failed!");
            }
        }

        public BotClient? GetClient()
        {
            return _botClient;
        }
    }
}
