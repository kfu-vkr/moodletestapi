using System;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MoodleTestAPI.Tools
{
    public class ControllerValidator
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<ControllerValidator> _logger;
        private readonly IConfiguration _configuration;
        public ControllerValidator(IServiceProvider services)
        {
            _services = services;
            using var scope = services.CreateScope();
            _logger = scope.ServiceProvider.GetRequiredService<ILogger<ControllerValidator>>();
            _configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();
        }

        public bool ValidateEvent(JsonElement value)
        {
            var eventName = value.GetFieldAsString("eventname").ToLower().Trim();
            if (!string.IsNullOrEmpty(eventName))
            {
                return false;
            }
            
            _logger.LogError("Error 500: invalid or empty eventname");
            return true;
        }
        
        public bool CheckAuth(JsonElement value)
        {
            var token = value.GetFieldAsString("token").ToLower().Trim();
            var appSettingsToken = _configuration.GetSection("AppSettings:ApiToken").Get<string>()
                .ToLower().Trim();

            if (!string.IsNullOrEmpty(token) && token.Equals(appSettingsToken))
            {
                return false;
            }
            
            _logger.LogError("Error 401: invalid or empty token");
            return true;
        }
        
    }
}