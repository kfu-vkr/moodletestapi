#nullable enable
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using MoodleTestAPI.Models;
using MoodleTestAPI.Models.Telegram;

namespace MoodleTestAPI.Tools
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public static class Extensions
    {
        /// <summary>
        /// Parse string value 'val' to boolean type if possible, otherwise return default value
        /// </summary>
        /// <param name="val">Source string value</param>
        /// <param name="defaultValue">Default bool value</param>
        /// <returns>boolean value</returns>
        public static bool ToBool(this string val, bool defaultValue = false)
        {
            return bool.TryParse(val, out var outval) ? outval : defaultValue;
        }

        public static TValue? ToReqDto<TValue>(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                throw new ArgumentNullException(nameof(s));
            }
            return JsonSerializer.Deserialize<TValue>(s);
        }

        public static string GetFieldAsString(this JsonElement jsonElement, string fieldName, string defaultValue = "")
        {
            var res = jsonElement.GetProperty(fieldName).GetString();
            return !string.IsNullOrEmpty(res) ? res : defaultValue;
        }

        public static string ToString(this JsonElement element, bool indent)
        {
            return element.ValueKind == JsonValueKind.Undefined 
                ? "" 
                : JsonSerializer.Serialize(element, new JsonSerializerOptions
                {
                    WriteIndented = indent
                });
        }

        public static string ToString(this TGUpdate element, bool indent = false, bool ignoreNulls = false)
        {
            return JsonSerializer.Serialize(element, new JsonSerializerOptions
            {
                IgnoreNullValues = ignoreNulls,
                WriteIndented = indent
            });
        }
        
        public static string ToString(this object? element, bool indent = false, bool ignoreNulls = false)
        {
            return JsonSerializer.Serialize(element, new JsonSerializerOptions
            {
                IgnoreNullValues = ignoreNulls,
                WriteIndented = indent
            });
        }
        
        public static string? ToMD5String(this string? s)
        {
            if (s == null)
            {
                return null;
            }

            var hashBytes = MD5.Create()
                    .ComputeHash(Encoding.UTF8.GetBytes(s))
                    .Select(x => x.ToString("x2")).ToArray();
            return string.Join("", hashBytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ci"></param>
        /// <returns></returns>
        // public static string? GetMD5String(this CourseInfo ci)
        // {
        //     return JsonSerializer.Serialize(ci).ToMD5String();
        // }
        //
        public static string? GetMD5String(this object ci)
        {
            return JsonSerializer.Serialize(ci).ToMD5String();
        }

        /// <summary>
        /// </summary>
        /// <param name="ci1"></param>
        /// <param name="ci2"></param>
        /// <returns></returns>
        public static bool EqualsCourse(this CourseInfo ci1, CourseInfo? ci2)
        {
            return ci2 != null && ci1.GetMD5String()!.Equals(ci2.GetMD5String());
        }

        /// <summary>
        /// Удаление HTML-тэгов из строки input
        /// </summary>
        /// <param name="input"></param>
        /// <param name="strictMode"></param>
        /// <returns></returns>
        public static string StripHTML(this string input, bool strictMode = true)
        {
            if (strictMode)
            {
                return Regex.Replace(input, "<.*?>", string.Empty);
            }

            var allowedTags = new[]
            {
                "b", "strong", 
                "i", "em", 
                "u", "ins",
                "s", "strike", "del",
                "br",  
            };

            input = Regex.Replace(input, $"</p>", $"</p><br/>", RegexOptions.IgnoreCase);
            
            foreach (var tag in allowedTags)
            {
                input = Regex.Replace(input, $"<{tag}>", $"&lt;{tag}&gt;", RegexOptions.IgnoreCase);
                input = Regex.Replace(input, $"</{tag}>", $"&lt;/{tag}&gt;", RegexOptions.IgnoreCase);
                input = Regex.Replace(input, $"<{tag}\\s*/>", $"&lt;{tag}/&gt;", RegexOptions.IgnoreCase);
            }
            
            input = Regex.Replace(input, "<.*?>", string.Empty);
            
            input = Regex.Replace(input, $"&lt;br/&gt;", Environment.NewLine);
            
            foreach (var tag in allowedTags)
            {
                input = Regex.Replace(input, $"&lt;{tag}&gt;", $"<{tag}>");
                input = Regex.Replace(input, $"&lt;/{tag}&gt;", $"</{tag}>");
                input = Regex.Replace(input, $"&lt;{tag}/&gt;", $"<{tag}/>");
            }

            return input;
        }
    }
}

