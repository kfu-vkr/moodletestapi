using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace MoodleTestAPI.Tools
{
    public static class HostBuilderTools
    {
        public static IHostBuilder CreateBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((ctx, config) =>
                {
                    config.AddEnvironmentVariables("ASPNETCORE_");
                    var env = ctx.HostingEnvironment;
                    var settingsDir = Path.Join(IOTools.GetRunningDir(env), "Settings");
                    if (!Directory.Exists(settingsDir))
                    {
                        return;
                    }

                    var reloadOnChange = ctx.Configuration.GetValue("hostBuilder:reloadConfigOnChange", true);
                    var jsonFiles0 = new[]
                    {
                        Path.Join(settingsDir, "appsettings.json"),
                        Path.Join(settingsDir, $"appsettings.{env.EnvironmentName}.json"),
                    };

                    var jsonFiles1 = Directory.GetFiles(settingsDir, "appsettings.*.json")
                        .Where(x => !jsonFiles0.Contains(x))
                        .ToArray();
                    Array.Sort(jsonFiles1);

                    foreach (var confFile in jsonFiles0.Union(jsonFiles1).ToArray())
                    {
                        config.AddJsonFile(confFile, true, reloadOnChange);
                    }
                });
        }
    }
}

