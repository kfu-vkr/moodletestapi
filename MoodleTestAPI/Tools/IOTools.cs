using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Hosting;

namespace MoodleTestAPI.Tools
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class IOTools
    {
        public static string? GetRunningDir(IHostEnvironment? env)
        {
            var ap = Assembly.GetEntryAssembly()!.Location;
            return string.IsNullOrEmpty(ap) ? env!.ContentRootPath : new FileInfo(ap).Directory?.FullName;
        }
    }
}