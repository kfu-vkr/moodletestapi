using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MoodleTestAPI.Tools
{
    public class IntFieldJsonConverter : JsonConverter<int>
    {
        public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            try
            {
                return reader.GetInt32();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                return int.TryParse(reader.GetString(), out var intVal) ? intVal : 0;
            }
            catch (Exception)
            {
                // ignored
            }

            return 0;
        }

        public override void Write(Utf8JsonWriter writer, int intValue, JsonSerializerOptions options)
        {
            writer.WriteNumberValue(intValue);
        }
    }
    
    public class IntNullableFieldJsonConverter : JsonConverter<int?>
    {
        public override int? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            try
            {
                return reader.GetInt32();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                return int.TryParse(reader.GetString(), out var intVal) ? intVal : null;
            }
            catch (Exception)
            {
                // ignored
            }

            return 0;
        }

        public override void Write(Utf8JsonWriter writer, int? intValue, JsonSerializerOptions options)
        {
            if (intValue.HasValue)
            {
                writer.WriteNumberValue(intValue.Value);                
            }
        }
    }
    
    public class BoolFieldJsonConverter : JsonConverter<bool>
    {
        public override bool Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            try
            {
                return reader.GetBoolean();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                return reader.GetInt32() == 1;
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                //return bool.TryParse(reader.GetString(), out var boolVal) && boolVal;
                var stringValue = reader.GetString();
                if (string.IsNullOrEmpty(stringValue))
                {
                    return false;
                }
                
                switch (stringValue.ToLower().Trim())
                {
                    case "1":
                    case "yes":
                    case "true":
                        return true;
                    default:
                        return false;
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }

        public override void Write(Utf8JsonWriter writer, bool boolValue, JsonSerializerOptions options)
        {
            writer.WriteBooleanValue(boolValue);                
        }
    }
    
    public class ParseDatetimeFieldJsonConverter : JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            double ts = 0;
            try
            {
                ts = reader.GetDouble();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                ts = reader.GetInt32();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                ts = (double)reader.GetDecimal();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                ts = reader.GetSingle();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                var sVal = reader.GetString();
                if (string.IsNullOrEmpty(sVal))
                {
                    throw new Exception();
                }
                ts = double.Parse(sVal);
            }
            catch (Exception)
            {
                // ignored
            }

            var unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, 
                System.DateTimeKind.Utc);
            var unixTimeStampInTicks = (long) (ts * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks, System.DateTimeKind.Utc);
        }

        public override void Write(Utf8JsonWriter writer, DateTime dtValue, JsonSerializerOptions options)
        {
            writer.WriteStringValue(dtValue);                
        }
    }
    
    public class ParseDatetimeNullableFieldJsonConverter : JsonConverter<DateTime?>
    {
        public override DateTime? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            double ts = 0;
            try
            {
                ts = reader.GetDouble();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                ts = reader.GetInt32();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                ts = (double)reader.GetDecimal();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                ts = reader.GetSingle();
            }
            catch (Exception)
            {
                // ignored
            }

            try
            {
                var sVal = reader.GetString();
                if (string.IsNullOrEmpty(sVal))
                {
                    throw new Exception();
                }
                ts = double.Parse(sVal);
            }
            catch (Exception)
            {
                // ignored
            }

            if (ts == 0)
            {
                return null;
            }

            var unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, 
                System.DateTimeKind.Utc);
            var unixTimeStampInTicks = (long) (ts * TimeSpan.TicksPerSecond);
            return new DateTime(unixStart.Ticks + unixTimeStampInTicks, System.DateTimeKind.Utc);
        }

        public override void Write(Utf8JsonWriter writer, DateTime? dtValue, JsonSerializerOptions options)
        {
            if (dtValue.HasValue)
            {
                writer.WriteStringValue(dtValue.Value);
            }
        }
    }
}
