using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace MoodleTestAPI.Tools
{
    public class JsonExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<JsonExceptionFilter> _logger;
        public JsonExceptionFilter(ILogger<JsonExceptionFilter> logger)
        {
            _logger = logger;
        }
        
        public void OnException(ExceptionContext context)
        {
            var ex = context.Exception;
            _logger.LogError(exception: ex, message: ex.Message);
            if (ex.InnerException != null)
            {
                _logger.LogError(exception: ex.InnerException, message: ex.InnerException.Message);
            }
            
            var result = new ObjectResult(new
            {
                error = true,
                exception = ex.Message,
                stacktrace = ex.StackTrace?
                    .Split(Environment.NewLine).Select(x => x.TrimEnd())
                    .ToList(),
                
                inner_exception = ex.InnerException?.Message,
                inner_stacktrace = ex.InnerException?.StackTrace?
                    .Split(Environment.NewLine).Select(x => x.TrimEnd())
                    .ToList(),
            });

            result.StatusCode = 500;
            context.Result = result;
        }
    }
}