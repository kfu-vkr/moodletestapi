using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using LiteDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Writers;
using MoodleTestAPI.Models;
using MoodleTestAPI.Models.Telegram;
using MoodleTestAPI.Tools;
using Telegram.BotAPI;
using VelocityDb.Exceptions;
using VelocityDb.Session;

#pragma warning disable 8602

namespace MoodleTestAPI
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "TemplateIsNotCompileTimeConstantProblem")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class VelocityDbService : IVelocityDbService
    {
        private readonly SessionNoServer _dbEngine;
        private readonly SessionPool _dbPool;
        private readonly IConfiguration _configuration;
        private readonly ILogger<VelocityDbService> _logger;
        private readonly IHostEnvironment _env;
        private readonly bool _initialized;

        #region Конструктор
        public VelocityDbService(
            IConfiguration configuration,
            ILogger<VelocityDbService> logger,
            IHostEnvironment environment
            )
        {
            _configuration = configuration;
            _logger = logger;
            _env = environment;
            _dbEngine = new SessionNoServer(configuration.GetValue("DBPath", "db"));
            _dbPool = new SessionPool(100, () => _dbEngine);

            if (_initialized)
            {
                return;
            }
            
            _dbEngine.BeginUpdate();
            _dbEngine.Commit();
            _initialized = true;                
        }
        #endregion
        
        #region Получение инстанса класса SessionPool
        public SessionPool GetPool()
        {
            return _dbPool;
        }
        #endregion

        #region FindCourse
        public CourseInfo? FindCourse(int? CourseId)
        {
            if (CourseId is null or <= 0)
            {
                return null;
            }

            CourseInfo? ci = null;
            var sessionId = 0;
            SessionBase? session = null;
            try
            {
                session = _dbPool.GetSession(out sessionId);
                session.BeginRead();
                ci = session.AllObjects<CourseInfo>()
                    .FirstOrDefault(x => x.CourseId == CourseId);
                session.Commit();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            finally
            {
                _dbPool.FreeSession(sessionId, session);
            }
            
            return ci;
        }
        #endregion

        #region UpdateCourse
        public void UpdateCourse(int CourseId, string? ShortName = default, string? FullName = default, 
            string? TGLinkUrl = default)
        {
            var courseInfo = new CourseInfo
            {
                CourseId = CourseId,
                ShortName = ShortName,
                FullName = FullName,
                TGLinkUrl = TGLinkUrl
            };
            
            UpdateCourse(courseInfo);
        }
        
        public void UpdateCourse(CourseInfo? courseInfo)
        {
            if (courseInfo is not {CourseId: > 0})
            {
                _logger.LogError("DB: Course info is null or CourseId is wrong");
                return;
            }

            var sessionId = 0;
            SessionBase? session = null;
            try
            {
                session = _dbPool.GetSession(out sessionId);
                session.BeginRead();
                var ci = session.AllObjects<CourseInfo>()
                    .FirstOrDefault(x => x.CourseId == courseInfo.CourseId);
                session.Commit();
                
                ci ??= courseInfo;

                session.BeginUpdate();
                _logger.LogInformation($"DB: Course \"{courseInfo.FullName}\" info updated");
                session.Persist(ci);
                session.Commit();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            finally
            {
                _dbPool.FreeSession(sessionId, session);
            }
        }
        #endregion

        #region RegisterTelegramChatLink
        public void RegisterTelegramChatLink(TGChatInfo chatInfo)
        {
            RegisterTelegramChatLink(chatInfo.ChatLink, chatInfo.Id, chatInfo.Type, chatInfo.Title);
        }
        
        public void RegisterTelegramChatLink(string? chatLink, long? chatId = default,
            string? type = default, string? title = default)
        {
            if (string.IsNullOrEmpty(chatLink))
            {
                return;
            }
                
            var dbDto = new TGChatInfo
            {
                ChatLink = chatLink,
                Id = chatId,
                Type = type,
                Title = title,
            };

            var sessionId = 0;
            SessionBase? session = null;
            try
            {
                session = _dbPool.GetSession(out sessionId);
                session.BeginRead();
                var chatSession = session.AllObjects<TGChatInfo>()
                    .FirstOrDefault(x => 
                        x.ChatLink.Equals(chatLink.Trim(), StringComparison.InvariantCultureIgnoreCase));
                session.Commit();
                
                chatSession ??= dbDto;
                
                session.BeginUpdate();
                session.Persist(chatSession);
                session.Commit();
                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            finally
            {
                _dbPool.FreeSession(sessionId, session);
            }
        }
        #endregion

        #region FindTelegramRegisteredChatLink
        public TGChatInfo? FindTelegramRegisteredChatLink(string? chatLink)
        {
            var sessionId = 0;
            SessionBase? session = null;
            try
            {
                session = _dbPool.GetSession(out sessionId);
                session.BeginRead();
                var ci = session.AllObjects<TGChatInfo>()
                    .FirstOrDefault(x =>
                        x.ChatLink.Equals(chatLink!.Trim(), StringComparison.InvariantCultureIgnoreCase));
                session.Commit();

                return ci;
            }
            finally
            {
                _dbPool.FreeSession(sessionId, session);
            }
        }
        #endregion

        public void FlushUpdates()
        {
            var sessionId = 0;
            SessionBase? session = null;
            try
            {
                // session = _dbPool.GetSession(out sessionId);
                // session.FlushUpdates();
                _dbEngine.FlushUpdates();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            finally
            {
                // _dbPool.FreeSession(sessionId, session);
            }
            
        }
    }
}
